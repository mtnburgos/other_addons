# -*- coding: utf-8 -*-

{
    'name': 'To-Do Applicatoin',
    'version': '0.1',
    'summary': 'Manage pending task',
    'sequence': 30,
    'description': """
Manage your personal To-Do
    """,
    'category': 'Productivity',
    'depends': ['base_setup'],
    'data': [
        'security/ir.model.access.csv',
        'security/todo_access_rules.xml',
        'views/todo_menu.xml',
        'views/todo_view.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
